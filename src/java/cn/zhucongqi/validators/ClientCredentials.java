/**
 * 
 */
package cn.zhucongqi.validators;

import javax.servlet.http.HttpServletRequest;

import cn.zhucongqi.exception.AuthProblemException;

/**
 * @author BruceZCQ [zcq@zhucongqi.cn]
 * @version
 */
public interface ClientCredentials<T extends HttpServletRequest> {
	
	/**
	 * validateClientCredentials
	 * @param request
	 * @throws AuthProblemException
	 */
	public void validateClientCredentials(T request) throws AuthProblemException;
}
