/**
 * 
 */
package cn.zhucongqi.validators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import cn.zhucongqi.consts.Consts;
import cn.zhucongqi.consts.ErrorConsts;
import cn.zhucongqi.exception.AuthProblemException;
import cn.zhucongqi.kit.AuthExceptionHandleKit;

import com.jfinal.kit.StrKit;

/**
 * 
 * @author BruceZCQ [zcq@zhucongqi.cn]
 * @version
 * @param <T>
 */
public abstract class AuthValidator<T extends HttpServletRequest> {

    protected List<String> requiredParams = new ArrayList<String>();
    protected HashMap<String, String> paramMustValues = new HashMap<String, String>();
    
    private ClientCredentials<T> customClientCredentialsValidator = null;
    
    public AuthValidator() {
        requiredParams.add(Consts.AuthConsts.AUTH_SCOPE);
        requiredParams.add(Consts.AuthConsts.AUTH_STATE);
        this.paramValuesValidation();
    }
    
    public void setCustomClientCredentialsValidator(ClientCredentials<T> customClientCredentialsValidator) {
    	this.customClientCredentialsValidator = customClientCredentialsValidator;
    }

    /**
     * param value validation 
     */
    public abstract void paramValuesValidation();
    
    /**
     * validate method
     * @param request
     * @throws AuthProblemException
     */
    protected void validateMethod(HttpServletRequest request) throws AuthProblemException {
        String method = request.getMethod();
        if (!method.equals(Consts.HttpMethod.GET) && !method.equals(Consts.HttpMethod.POST)) {
            throw AuthProblemException.error(ErrorConsts.CodeResponse.INVALID_REQUEST)
                .description("Method not correct.");
        }
    }

    /**
     * validate content type
     * @param request
     * @throws AuthProblemException
     */
    protected void validateContentType(T request) throws AuthProblemException {
        String contentType = request.getContentType();
        final String expectedContentType = Consts.ContentType.URL_ENCODED;
        if (!AuthExceptionHandleKit.hasContentType(contentType, expectedContentType)) {
            throw AuthExceptionHandleKit.handleBadContentTypeException(expectedContentType);
        }
    }

    /**
     * validate parameter
     * @param request
     * @throws AuthProblemException
     */
    protected void validateRequiredParameters(T request) throws AuthProblemException {
        final Set<String> missingParameters = new HashSet<String>();
        for (String requiredParam : requiredParams) {
            String val = request.getParameter(requiredParam);
            if (StrKit.isBlank(val)) {
                missingParameters.add(requiredParam);
            }
        }
        if (!missingParameters.isEmpty()) {
            throw AuthExceptionHandleKit.handleMissingParameters(missingParameters);
        }
    }
    
    /**
     * validate paramter values
     * @param request
     * @throws AuthProblemException
     */
    protected void validateRequiredParameterValues(T request) throws AuthProblemException {
    	final Set<String> keys = paramMustValues.keySet();
    	for (String key : keys) {
			String param = request.getParameter(key);
			String mustValue = paramMustValues.get(key);
			if (StrKit.notBlank(param) && !mustValue.equals(param)) {
				if (key.equals(Consts.AuthConsts.AUTH_RESPONSE_TYPE)) {
					throw AuthExceptionHandleKit.handleInvalidResponseTypeValueException(mustValue);
				} else if (key.endsWith(Consts.AuthConsts.AUTH_GRANT_TYPE)) {
					throw AuthExceptionHandleKit.handleInvalidGrantTypeValueException(mustValue);
				}
			}
		}
    }
    
    /**
     * validateClientCredentials
     * @param request
     * @throws AuthProblemException
     */
    protected void validateClientCredentials(T request)
			throws AuthProblemException {
    	// validate parameters missing
		Set<String> missingParameters = new HashSet<String>();
		
		String client_id = request.getParameter(Consts.AuthConsts.AUTH_CLIENT_ID);
		if (StrKit.isBlank(client_id)) {
			missingParameters.add(Consts.AuthConsts.AUTH_CLIENT_ID);
		}
		
		String client_secret = request.getParameter(Consts.AuthConsts.AUTH_CLIENT_SECRET);
		if (StrKit.isBlank(client_secret)) {
			missingParameters.add(Consts.AuthConsts.AUTH_CLIENT_SECRET);
		}

		//check missing or not
		if (!missingParameters.isEmpty()) {
			throw AuthExceptionHandleKit.handleMissingParameters(missingParameters);
		}
		
		//validate parameters validation
		this.customClientCredentialsValidator.validateClientCredentials(request);
	}
    
    /**
     * get client parameters
     * @param request
     */
    protected void getClientParameters(T request) {
    	
    }

    /**
     * validate request
     * 
     * @param request
     * @throws AuthProblemException
     */
    public void validate(T request) throws AuthProblemException {
        this.validateContentType(request);
        this.validateMethod(request);
        this.validateRequiredParameters(request);
        this.validateRequiredParameterValues(request);
        this.getClientParameters(request);
    }
    
}
